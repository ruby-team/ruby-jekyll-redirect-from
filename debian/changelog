ruby-jekyll-redirect-from (0.16.0-3) UNRELEASED; urgency=low

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Daniel Leidert ]
  * d/lintian-brush.conf: Set compat-release to oldstable to prevent dh
    bumps.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 18 Jul 2020 07:22:40 -0000

ruby-jekyll-redirect-from (0.16.0-2) unstable; urgency=medium

  * d/control (Build-Depends): Remove minimal version requirement for
    ruby-jekyll-sitemap. Use ruby-rspec to fix FTBFS (closes: #952023).
    (Depends): Use ${ruby:Depends} and remove interpreter and libraries.
  * d/ruby-tests.rake: Make it a file instead of a symlink and run tests.

 -- Daniel Leidert <dleidert@debian.org>  Sun, 23 Feb 2020 15:08:37 +0100

ruby-jekyll-redirect-from (0.16.0-1) unstable; urgency=medium

  * New upstream release.
  * d/compat: Removed.
  * d/control: Fix rules-requires-root-missing.
    (Standards-Version): Bump to 4.5.0.
    (Build-Depends): Add version for ruby-jekyll-sitemap and use
    debhelper-compat (fix uses-debhelper-compat-file).

 -- Daniel Leidert <dleidert@debian.org>  Sun, 02 Feb 2020 14:11:04 +0100

ruby-jekyll-redirect-from (0.15.0-2) unstable; urgency=medium

  * debian/control (Depends): Add proper jekyll dependency.
    (Build-Depends): Add jekyll, rake, ruby-jekyll-sitemap, ruby-rspec-core.
  * debian/ruby-tests.rake: Enable tests in spec/.
  * debian/patches/0002-Fix-tests.patch: Add patch to fix module loading.
  * debian/patches/series: Adjust the patch series accordingly.
  * debian/upstream/metadata: Add meta data.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 16 Aug 2019 15:59:29 +0200

ruby-jekyll-redirect-from (0.15.0-1) unstable; urgency=medium

  * Initial release. (Closes: #924065)

 -- Daniel Leidert <dleidert@debian.org>  Thu, 18 Jul 2019 04:08:01 +0200
